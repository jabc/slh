package edu.appcontest.jabc.studentslittlehelper;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuItemImpl;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bresidn O'Malley
 * 2016-3-28
 * This file handles the listview for the data from the csv file called bizdata.csv
 * This data will populate the listview and eventually open the map activity to show
 * data on the google map.
 */

public class BusinessDataList extends AppCompatActivity {

    //-- TYPE OF BUSINESS -------------------------------->
    /*
    * USE THESE TO CHOOSE TYPES OF BUSINESS
    */
    private String FOOD = "food/restaurants";
    private String AUTOMOTIVE = "automotive";
    private String BIKE = "bicycles";
    private String ENTERTAIN = "entertainment";
    private String BANK = "banking";
    private String FUN = "fun/entertainment";
    private String FIT = "health/fitness";
    private String HOTELS = "hotels";
    private String HOME = "house/home";
    private String TRAVEL = "travel";
    private String MOVING = "moving/storage";
    private String OTHER = "services  other";
    //------------------------------------------------------|

    //Constant Strings to -->
    public static final String PREF_NAME = "My Preferences";
    public static final String WIFI = "WIFI";
    public static final String DISC = "DISCOUNTS";
    //Shared Preferences-->
    private SharedPreferences sharedPref;


    private boolean wifi = true;
    private boolean discounts = true;
    private int count = 0;
    private Intent intent;
    private ListView listView = null;
    private ItemArrayAdapter itemArrayAdapter = null;
    private List<String[]> bizData;
    private List<String[]> bizListed;
    //int positionOfMenuItem = 0; // or whatever...

    MenuItemImpl wifiItem;
    MenuItemImpl discItem;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    //public String[] address;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_data_list);
        //-- shared prefs -->
        sharedPref = getSharedPreferences(PREF_NAME, MODE_PRIVATE);

        //if (listView == null) {
            listView = (ListView) findViewById(R.id.listView);

        //}
        //if (itemArrayAdapter == null) {
            itemArrayAdapter = new ItemArrayAdapter(getApplicationContext(), R.layout.item_layout);
        //}
        //listView.setOnItemClickListener(listItemClicked);
        bizListed = new ArrayList<String[]>();
        listView.setAdapter(itemArrayAdapter);
        intent = getIntent();
        wifi = intent.getBooleanExtra("wifi", wifi);
        discounts = intent.getBooleanExtra("discounts", discounts);
        readFile();
        setUpList();

       /* sWifi = new SpannableString("WiFi");
        sDisc = new SpannableString("Discounts");*/
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void readFile(){
        InputStream inputStream = getResources().openRawResource(R.raw.bizdata);
        CSVFile csvFile = new CSVFile(inputStream);
        bizData = csvFile.read();
    }

    public void setUpList() {
        /*if (listView == null) {
            listView = (ListView) findViewById(R.id.listView);
        }*/
        //Parcelable state = listView.onSaveInstanceState();

        //listView.onRestoreInstanceState(state);
        bizListed.clear();
        itemArrayAdapter.clear();
        count = 0;
        for (String[] bizList : bizData) {

            if(wifi == true && discounts == false){
                //-- Adds only items requested -->
                //Log.d("bizdata", bizList[2].toString());
                String a = bizList[2].toLowerCase().toString();
                if(a.equals("y") )
                {
                    //Log.d("equals", a);
                    itemArrayAdapter.add(bizList);
                    bizListed.add(bizList);
                } else {
                    //Log.d("Nope", a);
                }
            }
            else if(wifi == false && discounts == true){
                String a = bizList[3].toLowerCase().toString();
                if(a.equals("y") )
                {
                    //Log.d("equals", a);
                    itemArrayAdapter.add(bizList);
                    bizListed.add(bizList);
                } else {
                    //Log.d("Nope", a);
                }
            }
            else{
                itemArrayAdapter.add(bizList);
                bizListed.add(bizList);
            }

            //itemArrayAdapter.notifyDataSetChanged();
            //String a = bizList[4].toString();
            //address[count] = a;
            //itemArrayAdapter.add(bizList);
            //count++;
            //Log.d("count:", "count" + count);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //-- List object's address -->
                //Object itemClicked = parent.getAdapter().getItem(position);

                //Log.d("MAINACTIVITY:", "ItemClicked: " + bizData.get(position)[4]);
                String addr[] = {bizListed.get(position)[0], bizListed.get(position)[4], bizListed.get(position)[7]};
                //Uri gmmIntentUri = Uri.parse("geo:35.113281,-106.621216?q=" + addr);
                Intent mapIntent = new Intent(BusinessDataList.this, MapsActivity.class);
                //mapIntent.setPackage("com.google.android.apps.maps");
                //if (mapIntent.resolveActivity(getPackageManager()) != null) {
                mapIntent.putExtra("BizInfo", addr);
                startActivity(mapIntent);
                //}

                // Toast.makeText(BusinessDataList.this, "item: " + address[position].toString(), Toast.LENGTH_SHORT).show();
                /*
                * Create intent from position and send the address to maps
                *Intent passableData = new Intent(this, MapActivity.class);
                    passableData.putExtra("message", "Main activity says hello");
                startActivityForResult(passableData, 1);
                */
            }
        });

        listView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                /*
                * In theory this should make address textview selectable and copyable
                * But it doesnt....
                * */
                TextView address = (TextView) findViewById(R.id.tvAddress);
                address.setTextIsSelectable(true);
                address.setCursorVisible(true);
                return false;
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "BusinessDataList Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://edu.appcontest.jabc.studentslittlehelper/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "BusinessDataList Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://edu.appcontest.jabc.studentslittlehelper/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //mOptionsMenu = menu;
        getMenuInflater().inflate(R.menu.biz_menu, menu);

        wifiItem = (MenuItemImpl)menu.findItem(R.id.biz_wifi);
        discItem = (MenuItemImpl)menu.findItem(R.id.biz_disc);


        if(wifi == true && discounts == false){
            wifiItem.setEnabled(false);
            discItem.setEnabled(true);
        }
        else if(wifi == false && discounts == true){
            wifiItem.setEnabled(true);
            discItem.setEnabled(false);
        }
        else{
            wifiItem.setEnabled(true);
            discItem.setEnabled(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id)
        {
            case R.id.biz_disc:
                wifi = false;
                discounts = true;
                wifiItem.setEnabled(true);
                discItem.setEnabled(false);
                setUpList();
                return true;
            case R.id.biz_wifi:
                wifi = true;
                discounts = false;
                wifiItem.setEnabled(false);
                discItem.setEnabled(true);
                setUpList();
                return true;
            case R.id.biz_all:
                wifi = true;
                discounts = true;
                wifiItem.setEnabled(true);
                discItem.setEnabled(true);
                setUpList();
                return true;
            case R.id.biz_exit:
                this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
