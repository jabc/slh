package edu.appcontest.jabc.studentslittlehelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.Animation.AnimationListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {

    TextView logo1, logo2;
    Animation fade1, fade2, spinin;
    LayoutAnimationController controller;
    TableLayout table;
    TableRow row;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        logo1 = (TextView) findViewById(R.id.TextViewTopTitle);
        fade1 = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        logo1.startAnimation(fade1);
        logo2 = (TextView) findViewById(R.id.TextViewBottomTitle);
        fade2 = AnimationUtils.loadAnimation(this, R.anim.fade_in2);
        logo2.startAnimation(fade2);
        fade2.setAnimationListener(new AnimationListener() {
            //@Override
            public void onAnimationEnd(Animation arg0) {
                startActivity(new Intent(SplashActivity.this,
                        MenuActivity.class));
                SplashActivity.this.finish();
            }

            //@Override
            public void onAnimationRepeat(Animation arg0) {
            }

            //@Override
            public void onAnimationStart(Animation arg0) {
            }
        });

        spinin = AnimationUtils.loadAnimation(this, R.anim.custom_anim);
        controller = new LayoutAnimationController(spinin);
        table = (TableLayout) findViewById(R.id.TableLayout01);
        for (int i = 0; i < table.getChildCount(); i++) {
            row = (TableRow) table.getChildAt(i);
            row.setLayoutAnimation(controller);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        logo1.clearAnimation();
        logo2.clearAnimation();

        for (int i = 0; i < table.getChildCount(); i++) {
            row = (TableRow) table.getChildAt(i);
            row.clearAnimation();
        }
    }
}
