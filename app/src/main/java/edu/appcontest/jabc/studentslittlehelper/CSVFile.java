package edu.appcontest.jabc.studentslittlehelper;

import android.widget.LinearLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by b-rez on 3/28/16.
 * This file uses a bufferedreader to view a csv file and parse data into a list
 */
public class CSVFile {
    InputStream inputStream;

    public CSVFile(InputStream inputStream){
        //Read the incoming data path
        this.inputStream = inputStream;
    }

    public List read() {
        List resultList = new ArrayList();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try{
            String csvLine;
            while((csvLine = reader.readLine()) != null){
                String[] row = csvLine.split(",");
                resultList.add(row);
            }
        }catch(IOException ex){
            throw new RuntimeException("Error in reading CSV", ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                throw new RuntimeException("Error while closing Csv file", e);
            }
            return resultList;
        }
    }
}
