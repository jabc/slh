package edu.appcontest.jabc.studentslittlehelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by b-rez on 3/28/16.
 * This class creates the Biz data to use
 */
public class ItemArrayAdapter extends ArrayAdapter<String[]> {

    private List<String[]> bizList = new ArrayList<String[]>();

    static class ItemViewHolder {
        TextView name;
        TextView address;
        ImageView wifiImage;
        ImageView discountImage;

    }

    public ItemArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public void add(String[] object) {
        bizList.add(object);
        super.add(object);
    }

    public void clear(){
        bizList.clear();
    }

    @Override
    public int getCount() {
        return this.bizList.size();
    }

    @Override
    public String[] getItem(int index) {
        return this.bizList.get(index);
    }

    //get name
    //get address

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ItemViewHolder viewHolder;
        if (row == null) {
            //-- Inflate the layout -->
            LayoutInflater inflater = (LayoutInflater) this.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //-- Add row -->
            row = inflater.inflate(R.layout.item_layout, parent, false);

            //-- Hold the view duh -->
            viewHolder = new ItemViewHolder();

            //-- Just show name & address of business -->
            viewHolder.name = (TextView) row.findViewById(R.id.name);
            viewHolder.address = (TextView)row.findViewById(R.id.tvAddress);
            viewHolder.wifiImage = (ImageView) row.findViewById(R.id.imgWifi);
            viewHolder.discountImage = (ImageView) row.findViewById(R.id.imgDiscount);


            //viewHolder.score = (TextView) row.findViewById(R.id.score);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ItemViewHolder)row.getTag();
        }
        String[] business = getItem(position);
        String wifi = business[2].toLowerCase().toString();
        String discount = business[3].toLowerCase().toString();


        //-- Set name and address of biz -->
        viewHolder.name.setText(business[0]);
        viewHolder.address.setText(business[4]);
        //-- Set image of wifi or discount if availble at the biz -->
        if(wifi.equals("y")){
            viewHolder.wifiImage.setVisibility(View.VISIBLE);
        } else {
            viewHolder.wifiImage.setVisibility(View.INVISIBLE);
        }
        if(discount.equals("y")){
            viewHolder.discountImage.setVisibility(View.VISIBLE);
        }else {
            viewHolder.discountImage.setVisibility(View.INVISIBLE);
        }

        return row;
        // }
        // else {
        //   return null;
        // }

        //viewHolder.score.setText(stat[1]);

    }
}
