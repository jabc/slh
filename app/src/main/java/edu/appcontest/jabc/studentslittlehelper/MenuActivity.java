package edu.appcontest.jabc.studentslittlehelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by jmoralesowen on 3/14/2016.
 */
public class MenuActivity extends AppCompatActivity{
    ListView menuList;
    ArrayAdapter<String> adapt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        menuList = (ListView) findViewById(R.id.ListView_Menu);
        String[] items = {getResources().getString(R.string.menu_item_notes), getResources().getString(R.string.menu_item_routes), getResources().getString(R.string.menu_item_wifi), getResources().getString(R.string.menu_item_disc), getResources().getString(R.string.menu_item_calc)};
        adapt = new ArrayAdapter<String>(this, R.layout.menu_item, items);
        menuList.setAdapter(adapt);
        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                TextView textView = (TextView) itemClicked;
                String strText = textView.getText().toString();
                if (strText.equalsIgnoreCase(getResources().getString(R.string.menu_item_notes))) {
                    startActivity(new Intent(MenuActivity.this, NoteActivity.class));
                    //Toast.makeText(MenuActivity.this, "Notes menu item pressed", Toast.LENGTH_SHORT).show();
                } else if (strText.equalsIgnoreCase(getResources().getString(R.string.menu_item_routes))) {
                    startActivity(new Intent(MenuActivity.this, MapsActivity.class));
                    //Toast.makeText(MenuActivity.this, "Routes menu item pressed", Toast.LENGTH_SHORT).show();
                } else if (strText.equalsIgnoreCase(getResources().getString(R.string.menu_item_wifi))) {
                    Intent wifiIntent = new Intent(MenuActivity.this, BusinessDataList.class);
                    Bundle extras = new Bundle();
                    extras.putBoolean("wifi", true);
                    extras.putBoolean("discounts", false);
                    wifiIntent.putExtras(extras);
                    startActivity(wifiIntent);
                    //Toast.makeText(MenuActivity.this, "WiFi Spots menu item pressed", Toast.LENGTH_SHORT).show();
                } else if (strText.equalsIgnoreCase(getResources().getString(R.string.menu_item_disc))) {
                    Intent wifiIntent = new Intent(MenuActivity.this, BusinessDataList.class);
                    wifiIntent.putExtra("wifi", false);
                    wifiIntent.putExtra("discounts", true);
                    startActivity(wifiIntent);
                    //Toast.makeText(MenuActivity.this, "WiFi Spots menu item pressed", Toast.LENGTH_SHORT).show();
                }else if (strText.equalsIgnoreCase(getResources().getString(R.string.menu_item_calc))) {
                    startActivity(new Intent(MenuActivity.this, WeightedGradeMain.class));
                    //Toast.makeText(MenuActivity.this, "Grade Calculator menu item pressed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
