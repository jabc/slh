package edu.appcontest.jabc.studentslittlehelper;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.XmlResourceParser;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks, LocationListener  {

    private GoogleMap mMap;
    private String provider;
    private boolean permission;
    private URL is;
    //private String coord;
    private GoogleApiClient myClient;
    private Location myLastLoc;
    private double lat, lng;
    public static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 73;
    private Marker myMark, wdMark;
    private Intent intent;
    private Geocoder geocoder;
    private String[] addr;
    private List<Marker> buses = new ArrayList<Marker>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Create an instance of GoogleAPIClient.
        if (myClient == null) {
            myClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    //.addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        myClient.connect();

        intent = getIntent();

        geocoder = new Geocoder(this);

        /*LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setNumUpdates(1);
        request.setInterval(0);
        LocationServices.FusedLocationApi
                .requestLocationUpdates(myClient, request, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        Log.i("onMapReady", "Location: " + location);
                    }
                });*/
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        LocationRequest myRequest = new LocationRequest();
        myRequest.setInterval(10000);
        myRequest.setFastestInterval(5000);
        myRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        myLastLoc = LocationServices.FusedLocationApi.getLastLocation(
                myClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(myClient, myRequest, this);

        if (myLastLoc != null) {
            lat = myLastLoc.getLatitude();
            lng = myLastLoc.getLongitude();
            Log.i("onConnected", "Location: " + lat + "," + lng);
            LatLng loc = new LatLng(lat,lng);
            if(myMark != null) {
                myMark.remove();
            }
            myMark = mMap.addMarker(new MarkerOptions().position(loc).title("YOU").icon(BitmapDescriptorFactory.fromResource(R.drawable.avatar)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        addr = intent.getStringArrayExtra("BizInfo");
        if(addr != null) {
            new GetBizLocation().execute();
        }
        repeatingAsyncTask();

        Toast.makeText(this, "Tap on map marker for map options.", Toast.LENGTH_SHORT).show();
        /*
        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSION_ACCESS_COURSE_LOCATION);
            Toast.makeText(this, "requesting permission.", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "not requesting permission.", Toast.LENGTH_SHORT).show();
        }

        if(permission) {
            location = service.getLastKnownLocation(provider);

            // Add a marker at current location and move the camera
            if(location!=null) {
                LatLng here = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.addMarker(new MarkerOptions().position(here).title("You are here").icon(BitmapDescriptorFactory.fromResource(R.drawable.avatar)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(here));
            }
            else
            {
                Toast.makeText(this, "Unable to get the current location.", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            //Toast.makeText(this, "Location permission not granted. Unable to get the current location.", Toast.LENGTH_SHORT).show();
        }

        //mMap.setOnMyLocationChangeListener(myLocationChangeListener);*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    permission = true;

                } else {

                    permission = false;
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        myLastLoc = location;
        if (myLastLoc != null) {
            lat = myLastLoc.getLatitude();
            lng = myLastLoc.getLongitude();
            //Log.i("onConnected", "Location: " + lat + "," + lng);
            LatLng loc = new LatLng(lat,lng);
            if(myMark != null) {
                myMark.remove();
            }
            myMark = mMap.addMarker(new MarkerOptions().position(loc).title("YOU").icon(BitmapDescriptorFactory.fromResource(R.drawable.avatar)));
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
        }
    }

    private void repeatingAsyncTask() {

        final Handler handler = new Handler();
        Timer timer = new Timer();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                            new DownloadKmlFile().execute();
                    }
                });
            }
        };

        timer.schedule(task, 0, 15*1000);  // interval of 15 seconds

    }

    private class GetBizLocation extends AsyncTask<String, Void, Void> {
        LatLng loc;

        public GetBizLocation(){
        }

        @Override
        protected Void doInBackground(String... params) {

            String uri = "http://maps.google.com/maps/api/geocode/json?address=" + addr[1].replace(" ", "+") +"+" + addr[2] + "&sensor=false";
            Log.e("doInBackground", "in try after address: " + uri);
            HttpGet httpGet = new HttpGet(uri);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            StringBuilder stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());

                lng = ((JSONArray) jsonObject.get("results"))
                        .getJSONObject(0).getJSONObject("geometry")
                        .getJSONObject("location").getDouble("lng");

                lat = ((JSONArray) jsonObject.get("results"))
                        .getJSONObject(0).getJSONObject("geometry")
                        .getJSONObject("location").getDouble("lat");

                //Log.d("latitude", "" + lat);
                //Log.d("longitude", "" + lng);

                loc = new LatLng(lat,lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            /*List<Address> addresses;
            try {
                Log.e("doInBackground", "in try");
                addresses = geocoder.getFromLocationName(addr[0] + " " + addr[1] +" " + addr[2], 5, 34.985858, -106.799711, 35.201067, -106.485336);
                while(addresses.size() == 0)
                {
                    addresses = geocoder.getFromLocationName(addr[0] + " " + addr[1] +" " + addr[2], 5, 34.985858, -106.799711, 35.201067, -106.485336);
                }
                Log.e("doInBackground", "in try after address: " + addr[1] + " " + addr[2] + " found: " + addresses.size());
                if(addresses.size() > 0) {

                    Log.e("doInBackground", "in if");
                    double lat= addresses.get(0).getLatitude();
                    double lng= addresses.get(0).getLongitude();
                    loc = new LatLng(lat, lng);
                    Log.e("doInBackground", "Lat: " + lat +", Lng: " + lng);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("MapsActivity", "In onPostExecute");
            if(loc != null) {
                wdMark = mMap.addMarker(new MarkerOptions().position(loc).title(addr[0]).icon(BitmapDescriptorFactory.fromResource(R.drawable.targetmapmarker)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            }
        }
    }

    private class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
        String strCoord;
        String strRouteNum;
        List<String> allRoutes = new ArrayList<String>();
        List<Integer> routeNums = new ArrayList<Integer>();

        public DownloadKmlFile() {
        }
        XmlPullParser busInfo;
        protected byte[] doInBackground(String... params) {
            try {
                //Log.d("MapsActivity", "In doInBackground");
                is = new URL("http://data.cabq.gov/transit/realtime/route/allroutes.kml");

                busInfo = XmlPullParserFactory.newInstance().newPullParser();
                busInfo.setInput(is.openStream(), null);
                //Log.e("doInBackground$", "in try after parser");
            } catch (IOException e) {
                busInfo = null;
                Log.e("doInBackground", "IO Failure during pull parser initialization", e);
            }catch (XmlPullParserException e1) {
                busInfo = null;
                Log.e("doInBackground", "Failed to initialize pull parser", e1);
            }

            if (busInfo != null) {
                //Log.e("doInBackground$", "in if, bus info not null");
                try {
                    int eventType = -1;
                    while (eventType != XmlResourceParser.END_DOCUMENT) {
                        if (eventType == XmlResourceParser.START_TAG) {

                            // Get the name of the tag (eg questions or question)
                            String strName = busInfo.getName();

                            if (strName.equals("name")) {
                                eventType = busInfo.next();
                                if ( eventType == XmlPullParser.TEXT ) {
                                    strRouteNum = busInfo.getText();
                                    //Log.e("doInBackground$inIF", "Bus #?: " + strBusNum);
                                    routeNums.add(Integer.parseInt(strRouteNum));
                                }
                            }
                            else if (strName.equals("coordinates")) {
                                eventType = busInfo.next();
                                if ( eventType == XmlPullParser.TEXT ) {
                                    //Log.e("doInBackground", "Text: " + coordinates.getText());
                                    strCoord = busInfo.getText();
                                    allRoutes.add(strCoord);
                                }
                            }
                        }
                        eventType = busInfo.next();
                    }
                    //result = true;
                } catch (XmlPullParserException e) {
                    Log.e("doInBackground", "Pull Parser failure", e);
                } catch (IOException e) {
                    Log.e("doInBackground", "IO Exception parsing XML", e);
                }
            }
            return null;
        }

        protected void onPostExecute(byte[] byteArr) {

            for(int j = 0; j < buses.size(); j++) {
                (buses.get(j)).remove();
            }
            buses.removeAll(buses);
            for (int i = 0; i < allRoutes.size();i++) {
                String latLng[] = allRoutes.get(i).split(",");
                LatLng loc = new LatLng(Double.parseDouble(latLng[1]), Double.parseDouble(latLng[0]));
                //Log.e("inFor", "allRoutes size: " + allRoutes.size() + " busNums size: " + routeNums.size() + " buses size: " + buses.size());

                if(allRoutes.size() == routeNums.size()) {
                    //Log.e("onPostExecute", "in if statement");
                    buses.add(mMap.addMarker(new MarkerOptions().position(loc).title("Route #" + routeNums.get(i)).icon(BitmapDescriptorFactory.fromResource(R.drawable.busmapmarker))));
                    //mMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
