package edu.appcontest.jabc.studentslittlehelper;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.DoubleBuffer;
/**
 * Created by jmoralesowen on 3/30/2016.
 */
public class WeightedGradeMain extends AppCompatActivity{
    //buttons
    private Button addButton;
    private Button calcButton;
    private Button removeButton;
    private Button clearButton;
    private TableRow tableRow2;
    private TableRow tableRow3;
    private TableRow tableRow4;
    private TableRow tableRow5;
    private TableRow tableRow6;
    private TableRow tableRow7;
    private TableRow tableRow8;
    private TextView tvFinalGrade;
    private EditText etCategory1;
    private EditText etTotalPoints1;
    private EditText etPointsByYou1;
    private EditText etPercentage1;
    private EditText etTotalPoints2;
    private EditText etPointsByYou2;
    private EditText etPercentage2;
    private EditText etTotalPoints3;
    private EditText etPointsByYou3;
    private EditText etPercentage3;
    private EditText etTotalPoints4;
    private EditText etPointsByYou4;
    private EditText etPercentage4;
    private EditText etTotalPoints5;
    private EditText etPointsByYou5;
    private EditText etPercentage5;
    private EditText etTotalPoints6;
    private EditText etPointsByYou6;
    private EditText etPercentage6;
    private EditText etTotalPoints7;
    private EditText etPointsByYou7;
    private EditText etPercentage7;
    private EditText etTotalPoints8;
    private EditText etPointsByYou8;
    private EditText etPercentage8;
    private int count = 1;
    private double totalPoints = 0.0;
    private double pointsByYou = 0.0;
    private double percentage = 0.0;
    private double totalPoints2 = 0.0;
    private double pointsByYou2 = 0.0;
    private double percentage2 = 0.0;
    private double totalPoints3 = 0.0;
    private double pointsByYou3 = 0.0;
    private double percentage3 = 0.0;
    private double totalPoints4 = 0.0;
    private double pointsByYou4 = 0.0;
    private double percentage4 = 0.0;
    private double totalPoints5 = 0.0;
    private double pointsByYou5 = 0.0;
    private double percentage5 = 0.0;
    private double totalPoints6 = 0.0;
    private double pointsByYou6 = 0.0;
    private double percentage6 = 0.0;
    private double totalPoints7 = 0.0;
    private double pointsByYou7 = 0.0;
    private double percentage7 = 0.0;
    private double totalPoints8 = 0.0;
    private double pointsByYou8 = 0.0;
    private double percentage8 = 0.0;
    private double finalGrade = 0.0;
    private boolean checkVal = false;
    private double totPercent = 0.0;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weighted_grade_main);
        addButton = (Button) findViewById(R.id.btnAddCategory);
        calcButton = (Button)findViewById(R.id.btnCalcGrade);
        removeButton = (Button)findViewById(R.id.btnRemove);
        clearButton = (Button)findViewById(R.id.btnClear);
        addButton.setOnClickListener(onClick());
        calcButton.setOnClickListener(onClickListener());
        removeButton.setOnClickListener(onClickRemove());
        clearButton.setOnClickListener(onClickClear());
        tableRow2 = (TableRow)findViewById(R.id.TR2);
        tableRow3 = (TableRow)findViewById(R.id.TR3);
        tableRow4 = (TableRow)findViewById(R.id.TR4);
        tableRow5 = (TableRow)findViewById(R.id.TR5);
        tableRow6 = (TableRow)findViewById(R.id.TR6);
        tableRow7 = (TableRow)findViewById(R.id.TR7);
        tableRow8 = (TableRow)findViewById(R.id.TR8);
        tvFinalGrade = (TextView)findViewById(R.id.finalGrade);
        etCategory1 = (EditText)findViewById(R.id.etCategory);
        etTotalPoints1 = (EditText)findViewById(R.id.etTotalPoints);
        etPointsByYou1 = (EditText)findViewById(R.id.etPointsByYou);
        etPercentage1 = (EditText)findViewById(R.id.etPercentage);
        etTotalPoints2 = (EditText)findViewById(R.id.etTotalPoints2);
        etPointsByYou2 = (EditText)findViewById(R.id.etPointsByYou2);
        etPercentage2 = (EditText)findViewById(R.id.etPercentage2);
        etTotalPoints3 = (EditText)findViewById(R.id.etTotalPoints3);
        etPointsByYou3 = (EditText)findViewById(R.id.etPointsByYou3);
        etPercentage3 = (EditText)findViewById(R.id.etPercentage3);
        etTotalPoints4 = (EditText)findViewById(R.id.etTotalPoints4);
        etPointsByYou4 = (EditText)findViewById(R.id.etPointsByYou4);
        etPercentage4 = (EditText)findViewById(R.id.etPercentage4);
        etTotalPoints5 = (EditText)findViewById(R.id.etTotalPoints5);
        etPointsByYou5 = (EditText)findViewById(R.id.etPointsByYou5);
        etPercentage5 = (EditText)findViewById(R.id.etPercentage5);
        etTotalPoints6 = (EditText)findViewById(R.id.etTotalPoints6);
        etPointsByYou6 = (EditText)findViewById(R.id.etPointsByYou6);
        etPercentage6 = (EditText)findViewById(R.id.etPercentage6);
        etTotalPoints7 = (EditText)findViewById(R.id.etTotalPoints7);
        etPointsByYou7 = (EditText)findViewById(R.id.etPointsByYou7);
        etPercentage7 = (EditText)findViewById(R.id.etPercentage7);
        etTotalPoints8 = (EditText)findViewById(R.id.etTotalPoints8);
        etPointsByYou8 = (EditText)findViewById(R.id.etPointsByYou8);
        etPercentage8 = (EditText)findViewById(R.id.etPercentage8);

    }


    //Onclicklistener to add additional categories
    private View.OnClickListener onClick()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                count++;
                createOneFullTableRow(count);
            }
        };
    }

    //calls checkvalues to ensure all edittext boxes have values
    //calls calculate to calculate grade and sets text to result text box
    private View.OnClickListener onClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                checkVal = true;
                checkValues(count);
                //Toast.makeText(WeightedGradeMain.this, checkVal + "", Toast.LENGTH_SHORT).show();
                if(checkVal)
                {
                    calculate();
                    //Toast.makeText(WeightedGradeMain.this, finalGrade + "", Toast.LENGTH_SHORT).show();
                    if(finalGrade != 0)
                    {
                        int a = (int)totPercent;
                        if(a != 100)
                        {
                            /*
                            * Fixed to take an int to stop crash
                            * */
                            Log.d("Weighted Grade", Integer.toString(a));
                            tvFinalGrade.setText("Your grade is: " + Double.toString(getFinalGrade()) + "%" + "\r\nGrade may be incorrect. Total percentage does not equal 100%.");
                        } else {
                            tvFinalGrade.setText("Your grade is: " + Double.toString(getFinalGrade()) + "%");
                        }
                    }
                    else
                    {
                        tvFinalGrade.setText("Please ensure you entered the correct information in all places");
                    }
                }
                else
                {
                    tvFinalGrade.setText("Please ensure you entered the correct information in all places");
                }
            }
        };
    }

    //removes one table row at a time
    private View.OnClickListener onClickRemove()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                removeOneFullTableRow(count);
                count--;
            }
        };
    }

    //calls clear to remove all categories but one
    private View.OnClickListener onClickClear()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                clearTableRow(count);
                tvFinalGrade.setText("");
                etCategory1.setHint(R.string.catHintText);
                etTotalPoints1.setHint(R.string.totalPointsHint);
                etPointsByYou1.setHint(R.string.pointsByYouHint);
                etPercentage1.setHint(R.string.percentageHint);
                etCategory1.setText("");
                etTotalPoints1.setText("");
                etPointsByYou1.setText("");
                etPercentage1.setText("");

                totalPoints = 0.0;
                pointsByYou = 0.0;
                percentage = 0.0;
                totalPoints2 = 0.0;
                pointsByYou2 = 0.0;
                percentage2 = 0.0;
                totalPoints3 = 0.0;
                pointsByYou3 = 0.0;
                percentage3 = 0.0;
                totalPoints4 = 0.0;
                pointsByYou4 = 0.0;
                percentage4 = 0.0;
                totalPoints5 = 0.0;
                pointsByYou5 = 0.0;
                percentage5 = 0.0;
                totalPoints6 = 0.0;
                pointsByYou6 = 0.0;
                percentage6 = 0.0;
                totalPoints7 = 0.0;
                pointsByYou7 = 0.0;
                percentage7 = 0.0;
                totalPoints8 = 0.0;
                pointsByYou8 = 0.0;
                percentage8 = 0.0;
                checkVal = false;
            }
        };
    }

    //turns on one table row at a time
    private void createOneFullTableRow(int count)
    {
        switch(count)
        {
            case 2:
                tableRow2.setVisibility(View.VISIBLE);
                break;
            case 3:
                tableRow3.setVisibility(View.VISIBLE);
                break;
            case 4:
                tableRow4.setVisibility(View.VISIBLE);
                break;
            case 5:
                tableRow5.setVisibility(View.VISIBLE);
                break;
            case 6:
                tableRow6.setVisibility(View.VISIBLE);
                break;
            case 7:
                tableRow7.setVisibility(View.VISIBLE);
                break;
            case 8:
                tableRow8.setVisibility(View.VISIBLE);
                break;
        }
    }

    //checks that edit text boxes have values
    private boolean checkValues( int count)
    {
        switch(count)
        {
            case 1:
                if((TextUtils.isEmpty(etTotalPoints1.getText().toString()))||(TextUtils.isEmpty(etPointsByYou1.getText().toString()))||
                        (TextUtils.isEmpty(etPercentage1.getText().toString())))
                {
                    checkVal = false;
                }
                else
                {
                    totalPoints = Double.parseDouble(etTotalPoints1.getText().toString());
                    pointsByYou = Double.parseDouble(etPointsByYou1.getText().toString());
                    percentage = Double.parseDouble(etPercentage1.getText().toString());
                    checkVal = true;
                }
                break;
            case 2:
                if((TextUtils.isEmpty(etTotalPoints1.getText())) || (TextUtils.isEmpty(etPointsByYou1.getText()))||
                        (TextUtils.isEmpty(etPercentage1.getText())) ||
                        (TextUtils.isEmpty(etTotalPoints2.getText()))||(TextUtils.isEmpty(etPointsByYou2.getText()))||
                        (TextUtils.isEmpty(etPercentage2.getText())))
                {
                    checkVal = false;
                }
                else
                {
                    checkVal = true;
                    //getting category 1 text values
                    totalPoints = Double.parseDouble(etTotalPoints1.getText().toString());
                    pointsByYou = Double.parseDouble(etPointsByYou1.getText().toString());
                    percentage = Double.parseDouble(etPercentage1.getText().toString());

                    //getting category 2 text values for calculations
                    totalPoints2 = Double.parseDouble(etTotalPoints2.getText().toString());
                    pointsByYou2 = Double.parseDouble(etPointsByYou2.getText().toString());
                    percentage2 = Double.parseDouble(etPercentage2.getText().toString());

                }
                break;

            case 3:
                if((TextUtils.isEmpty(etTotalPoints1.getText())) || (TextUtils.isEmpty(etPointsByYou1.getText()))||
                        (TextUtils.isEmpty(etPercentage1.getText())) || (TextUtils.isEmpty(etTotalPoints2.getText()))||
                        (TextUtils.isEmpty(etPointsByYou2.getText()))|| (TextUtils.isEmpty(etPercentage2.getText()))||
                        (TextUtils.isEmpty(etTotalPoints3.getText()))|| (TextUtils.isEmpty(etPointsByYou3.getText()))||
                        (TextUtils.isEmpty(etPercentage3.getText())))
                {
                    checkVal = false;
                }
                else
                {
                    //category 1
                    totalPoints = Double.parseDouble(etTotalPoints1.getText().toString());
                    pointsByYou = Double.parseDouble(etPointsByYou1.getText().toString());
                    percentage = Double.parseDouble(etPercentage1.getText().toString());

                    //category 2
                    totalPoints2 = Double.parseDouble(etTotalPoints2.getText().toString());
                    pointsByYou2 = Double.parseDouble(etPointsByYou2.getText().toString());
                    percentage2 = Double.parseDouble(etPercentage2.getText().toString());

                    //category 3
                    totalPoints3 = Double.parseDouble(etTotalPoints3.getText().toString());
                    pointsByYou3 = Double.parseDouble(etPointsByYou3.getText().toString());
                    percentage3 = Double.parseDouble(etPercentage3.getText().toString());
                }
                break;
            case 4:
                if((TextUtils.isEmpty(etTotalPoints1.getText())) || (TextUtils.isEmpty(etPointsByYou1.getText()))||
                        (TextUtils.isEmpty(etPercentage1.getText())) || (TextUtils.isEmpty(etTotalPoints2.getText()))||
                        (TextUtils.isEmpty(etPointsByYou2.getText()))|| (TextUtils.isEmpty(etPercentage2.getText()))||
                        (TextUtils.isEmpty(etTotalPoints3.getText()))|| (TextUtils.isEmpty(etPointsByYou3.getText()))||
                        (TextUtils.isEmpty(etPercentage3.getText()))||  (TextUtils.isEmpty(etTotalPoints4.getText()))||
                        (TextUtils.isEmpty(etPointsByYou4.getText()))|| (TextUtils.isEmpty(etPercentage4.getText())))
                {
                    checkVal = false;
                }
                else
                {
                    //category 1
                    totalPoints = Double.parseDouble(etTotalPoints1.getText().toString());
                    pointsByYou = Double.parseDouble(etPointsByYou1.getText().toString());
                    percentage = Double.parseDouble(etPercentage1.getText().toString());

                    //category 2
                    totalPoints2 = Double.parseDouble(etTotalPoints2.getText().toString());
                    pointsByYou2 = Double.parseDouble(etPointsByYou2.getText().toString());
                    percentage2 = Double.parseDouble(etPercentage2.getText().toString());

                    //category 3
                    totalPoints3 = Double.parseDouble(etTotalPoints3.getText().toString());
                    pointsByYou3 = Double.parseDouble(etPointsByYou3.getText().toString());
                    percentage3 = Double.parseDouble(etPercentage3.getText().toString());

                    //category 4
                    totalPoints4 = Double.parseDouble(etTotalPoints4.getText().toString());
                    pointsByYou4 = Double.parseDouble(etPointsByYou4.getText().toString());
                    percentage4 = Double.parseDouble(etPercentage4.getText().toString());
                }
                break;

            case 5:
                if((TextUtils.isEmpty(etTotalPoints1.getText())) || (TextUtils.isEmpty(etPointsByYou1.getText()))||
                        (TextUtils.isEmpty(etPercentage1.getText())) || (TextUtils.isEmpty(etTotalPoints2.getText()))||
                        (TextUtils.isEmpty(etPointsByYou2.getText()))|| (TextUtils.isEmpty(etPercentage2.getText()))||
                        (TextUtils.isEmpty(etTotalPoints3.getText()))|| (TextUtils.isEmpty(etPointsByYou3.getText()))||
                        (TextUtils.isEmpty(etPercentage3.getText()))||  (TextUtils.isEmpty(etTotalPoints4.getText()))||
                        (TextUtils.isEmpty(etPointsByYou4.getText()))|| (TextUtils.isEmpty(etPercentage4.getText()))||
                        (TextUtils.isEmpty(etTotalPoints5.getText()))|| (TextUtils.isEmpty(etPointsByYou5.getText()))||
                        (TextUtils.isEmpty(etPercentage5.getText())))
                {
                    checkVal = false;
                }
                else
                {
                    //category 1
                    totalPoints = Double.parseDouble(etTotalPoints1.getText().toString());
                    pointsByYou = Double.parseDouble(etPointsByYou1.getText().toString());
                    percentage = Double.parseDouble(etPercentage1.getText().toString());

                    //category 2
                    totalPoints2 = Double.parseDouble(etTotalPoints2.getText().toString());
                    pointsByYou2 = Double.parseDouble(etPointsByYou2.getText().toString());
                    percentage2 = Double.parseDouble(etPercentage2.getText().toString());

                    //category 3
                    totalPoints3 = Double.parseDouble(etTotalPoints3.getText().toString());
                    pointsByYou3 = Double.parseDouble(etPointsByYou3.getText().toString());
                    percentage3 = Double.parseDouble(etPercentage3.getText().toString());

                    //category 4
                    totalPoints4 = Double.parseDouble(etTotalPoints4.getText().toString());
                    pointsByYou4 = Double.parseDouble(etPointsByYou4.getText().toString());
                    percentage4 = Double.parseDouble(etPercentage4.getText().toString());

                    //category 5
                    totalPoints5 = Double.parseDouble(etTotalPoints5.getText().toString());
                    pointsByYou5 = Double.parseDouble(etPointsByYou5.getText().toString());
                    percentage5 = Double.parseDouble(etPercentage5.getText().toString());
                }
                break;

            case 6:
                if((TextUtils.isEmpty(etTotalPoints1.getText())) || (TextUtils.isEmpty(etPointsByYou1.getText()))||
                        (TextUtils.isEmpty(etPercentage1.getText())) || (TextUtils.isEmpty(etTotalPoints2.getText()))||
                        (TextUtils.isEmpty(etPointsByYou2.getText()))|| (TextUtils.isEmpty(etPercentage2.getText()))||
                        (TextUtils.isEmpty(etTotalPoints3.getText()))|| (TextUtils.isEmpty(etPointsByYou3.getText()))||
                        (TextUtils.isEmpty(etPercentage3.getText()))||  (TextUtils.isEmpty(etTotalPoints4.getText()))||
                        (TextUtils.isEmpty(etPointsByYou4.getText()))|| (TextUtils.isEmpty(etPercentage4.getText()))||
                        (TextUtils.isEmpty(etTotalPoints5.getText()))|| (TextUtils.isEmpty(etPointsByYou5.getText()))||
                        (TextUtils.isEmpty(etPercentage5.getText()))||  (TextUtils.isEmpty(etTotalPoints6.getText()))||
                        (TextUtils.isEmpty(etPointsByYou6.getText()))|| (TextUtils.isEmpty(etPercentage6.getText())))
                {
                    checkVal = false;
                }
                else
                {
                    //category 1
                    totalPoints = Double.parseDouble(etTotalPoints1.getText().toString());
                    pointsByYou = Double.parseDouble(etPointsByYou1.getText().toString());
                    percentage = Double.parseDouble(etPercentage1.getText().toString());

                    //category 2
                    totalPoints2 = Double.parseDouble(etTotalPoints2.getText().toString());
                    pointsByYou2 = Double.parseDouble(etPointsByYou2.getText().toString());
                    percentage2 = Double.parseDouble(etPercentage2.getText().toString());

                    //category 3
                    totalPoints3 = Double.parseDouble(etTotalPoints3.getText().toString());
                    pointsByYou3 = Double.parseDouble(etPointsByYou3.getText().toString());
                    percentage3 = Double.parseDouble(etPercentage3.getText().toString());

                    //category 4
                    totalPoints4 = Double.parseDouble(etTotalPoints4.getText().toString());
                    pointsByYou4 = Double.parseDouble(etPointsByYou4.getText().toString());
                    percentage4 = Double.parseDouble(etPercentage4.getText().toString());

                    //category 5
                    totalPoints5 = Double.parseDouble(etTotalPoints5.getText().toString());
                    pointsByYou5 = Double.parseDouble(etPointsByYou5.getText().toString());
                    percentage5 = Double.parseDouble(etPercentage5.getText().toString());

                    //category 6
                    totalPoints6 = Double.parseDouble(etTotalPoints6.getText().toString());
                    pointsByYou6 = Double.parseDouble(etPointsByYou6.getText().toString());
                    percentage6 = Double.parseDouble(etPercentage6.getText().toString());
                }
                break;

            case 7:
                if((TextUtils.isEmpty(etTotalPoints1.getText())) || (TextUtils.isEmpty(etPointsByYou1.getText()))||
                        (TextUtils.isEmpty(etPercentage1.getText())) || (TextUtils.isEmpty(etTotalPoints2.getText()))||
                        (TextUtils.isEmpty(etPointsByYou2.getText()))|| (TextUtils.isEmpty(etPercentage2.getText()))||
                        (TextUtils.isEmpty(etTotalPoints3.getText()))|| (TextUtils.isEmpty(etPointsByYou3.getText()))||
                        (TextUtils.isEmpty(etPercentage3.getText()))||  (TextUtils.isEmpty(etTotalPoints4.getText()))||
                        (TextUtils.isEmpty(etPointsByYou4.getText()))|| (TextUtils.isEmpty(etPercentage4.getText()))||
                        (TextUtils.isEmpty(etTotalPoints5.getText()))|| (TextUtils.isEmpty(etPointsByYou5.getText()))||
                        (TextUtils.isEmpty(etPercentage5.getText()))||  (TextUtils.isEmpty(etTotalPoints6.getText()))||
                        (TextUtils.isEmpty(etPointsByYou6.getText()))|| (TextUtils.isEmpty(etPercentage6.getText())) ||
                        (TextUtils.isEmpty(etTotalPoints7.getText()))|| (TextUtils.isEmpty(etPointsByYou7.getText()))||
                        (TextUtils.isEmpty(etPercentage7.getText())))
                {
                    checkVal = false;
                }
                else
                {
                    //category 1
                    totalPoints = Double.parseDouble(etTotalPoints1.getText().toString());
                    pointsByYou = Double.parseDouble(etPointsByYou1.getText().toString());
                    percentage = Double.parseDouble(etPercentage1.getText().toString());

                    //category 2
                    totalPoints2 = Double.parseDouble(etTotalPoints2.getText().toString());
                    pointsByYou2 = Double.parseDouble(etPointsByYou2.getText().toString());
                    percentage2 = Double.parseDouble(etPercentage2.getText().toString());

                    //category 3
                    totalPoints3 = Double.parseDouble(etTotalPoints3.getText().toString());
                    pointsByYou3 = Double.parseDouble(etPointsByYou3.getText().toString());
                    percentage3 = Double.parseDouble(etPercentage3.getText().toString());

                    //category 4
                    totalPoints4 = Double.parseDouble(etTotalPoints4.getText().toString());
                    pointsByYou4 = Double.parseDouble(etPointsByYou4.getText().toString());
                    percentage4 = Double.parseDouble(etPercentage4.getText().toString());

                    //category 5
                    totalPoints5 = Double.parseDouble(etTotalPoints5.getText().toString());
                    pointsByYou5 = Double.parseDouble(etPointsByYou5.getText().toString());
                    percentage5 = Double.parseDouble(etPercentage5.getText().toString());

                    //category 6
                    totalPoints6 = Double.parseDouble(etTotalPoints6.getText().toString());
                    pointsByYou6 = Double.parseDouble(etPointsByYou6.getText().toString());
                    percentage6 = Double.parseDouble(etPercentage6.getText().toString());

                    //category 7
                    totalPoints7 = Double.parseDouble(etTotalPoints7.getText().toString());
                    pointsByYou7 = Double.parseDouble(etPointsByYou7.getText().toString());
                    percentage7 = Double.parseDouble(etPercentage7.getText().toString());
                }
                break;

            case 8:
                if((TextUtils.isEmpty(etTotalPoints1.getText())) || (TextUtils.isEmpty(etPointsByYou1.getText()))||
                        (TextUtils.isEmpty(etPercentage1.getText())) || (TextUtils.isEmpty(etTotalPoints2.getText()))||
                        (TextUtils.isEmpty(etPointsByYou2.getText()))|| (TextUtils.isEmpty(etPercentage2.getText()))||
                        (TextUtils.isEmpty(etTotalPoints3.getText()))|| (TextUtils.isEmpty(etPointsByYou3.getText()))||
                        (TextUtils.isEmpty(etPercentage3.getText()))||  (TextUtils.isEmpty(etTotalPoints4.getText()))||
                        (TextUtils.isEmpty(etPointsByYou4.getText()))|| (TextUtils.isEmpty(etPercentage4.getText()))||
                        (TextUtils.isEmpty(etTotalPoints5.getText()))|| (TextUtils.isEmpty(etPointsByYou5.getText()))||
                        (TextUtils.isEmpty(etPercentage5.getText()))||  (TextUtils.isEmpty(etTotalPoints6.getText()))||
                        (TextUtils.isEmpty(etPointsByYou6.getText()))|| (TextUtils.isEmpty(etPercentage6.getText())) ||
                        (TextUtils.isEmpty(etTotalPoints7.getText()))|| (TextUtils.isEmpty(etPointsByYou7.getText()))||
                        (TextUtils.isEmpty(etPercentage7.getText()))||  (TextUtils.isEmpty(etTotalPoints8.getText()))||
                        (TextUtils.isEmpty(etPointsByYou8.getText()))|| (TextUtils.isEmpty(etPercentage8.getText())))
                {
                    checkVal = false;
                }
                else
                {
                    //category 1
                    totalPoints = Double.parseDouble(etTotalPoints1.getText().toString());
                    pointsByYou = Double.parseDouble(etPointsByYou1.getText().toString());
                    percentage = Double.parseDouble(etPercentage1.getText().toString());

                    //category 2
                    totalPoints2 = Double.parseDouble(etTotalPoints2.getText().toString());
                    pointsByYou2 = Double.parseDouble(etPointsByYou2.getText().toString());
                    percentage2 = Double.parseDouble(etPercentage2.getText().toString());

                    //category 3
                    totalPoints3 = Double.parseDouble(etTotalPoints3.getText().toString());
                    pointsByYou3 = Double.parseDouble(etPointsByYou3.getText().toString());
                    percentage3 = Double.parseDouble(etPercentage3.getText().toString());

                    //category 4
                    totalPoints4 = Double.parseDouble(etTotalPoints4.getText().toString());
                    pointsByYou4 = Double.parseDouble(etPointsByYou4.getText().toString());
                    percentage4 = Double.parseDouble(etPercentage4.getText().toString());

                    //category 5
                    totalPoints5 = Double.parseDouble(etTotalPoints5.getText().toString());
                    pointsByYou5 = Double.parseDouble(etPointsByYou5.getText().toString());
                    percentage5 = Double.parseDouble(etPercentage5.getText().toString());

                    //category 6
                    totalPoints6 = Double.parseDouble(etTotalPoints6.getText().toString());
                    pointsByYou6 = Double.parseDouble(etPointsByYou6.getText().toString());
                    percentage6 = Double.parseDouble(etPercentage6.getText().toString());

                    //category 7
                    totalPoints7 = Double.parseDouble(etTotalPoints7.getText().toString());
                    pointsByYou7 = Double.parseDouble(etPointsByYou7.getText().toString());
                    percentage7 = Double.parseDouble(etPercentage7.getText().toString());

                    //category 8
                    totalPoints8 = Double.parseDouble(etTotalPoints8.getText().toString());
                    pointsByYou8 = Double.parseDouble(etPointsByYou8.getText().toString());
                    percentage8 = Double.parseDouble(etPercentage8.getText().toString());
                }
                break;
        }
        return checkVal;
    }

    //calculates the grade
    private void calculate()
    {
        finalGrade = 0;
        switch(count)
        {
            case 0:
                totalPoints = 0;
                pointsByYou = 0;
                percentage = 0;
                finalGrade = 0;
                break;
            case 1:
                if(!checkVal)
                {
                    totalPoints = 0;
                    pointsByYou = 0;
                    percentage = 0;
                    finalGrade = 0;
                }
                else
                {
                    finalGrade = (pointsByYou / totalPoints) * (percentage);
                    totPercent = percentage;
                }
                break;
            case 2:
                if(!checkVal)
                {
                    totalPoints = 0;
                    pointsByYou = 0;
                    percentage = 0;
                    totalPoints2 = 0;
                    pointsByYou2 = 0;
                    percentage2 = 0;
                    finalGrade = 0;
                }
                else
                {
                    //category 1
                    finalGrade = (pointsByYou / totalPoints) * (percentage);

                    //category 2
                    finalGrade += (pointsByYou2 / totalPoints2) * (percentage2);

                    totPercent = percentage + percentage2;
                }
                break;
            case 3:
                if(!checkVal)
                {
                    totalPoints = 0;
                    pointsByYou = 0;
                    percentage = 0;
                    totalPoints2 = 0;
                    pointsByYou2 = 0;
                    percentage2 = 0;
                    totalPoints3 = 0;
                    pointsByYou3 = 0;
                    percentage3 = 0;
                    finalGrade = 0;
                }
                else
                {
                    //category 1
                    finalGrade = (pointsByYou / totalPoints) * (percentage);

                    //category 2
                    finalGrade += (pointsByYou2 / totalPoints2) * (percentage2);

                    //category 3
                    finalGrade += (pointsByYou3 / totalPoints3) * (percentage3);

                    totPercent = percentage + percentage2 + percentage3;
                }
                break;
            case 4:
                if(!checkVal)
                {
                    totalPoints = 0;
                    pointsByYou = 0;
                    percentage = 0;
                    totalPoints2 = 0;
                    pointsByYou2 = 0;
                    percentage2 = 0;
                    totalPoints3 = 0;
                    pointsByYou3 = 0;
                    percentage3 = 0;
                    totalPoints4 = 0;
                    pointsByYou4 = 0;
                    percentage4 = 0;
                    finalGrade = 0;
                }
                else
                {
                    //category 1
                    finalGrade = (pointsByYou / totalPoints) * (percentage);

                    //category 2
                    finalGrade += (pointsByYou2 / totalPoints2) * (percentage2);

                    //category 3
                    finalGrade += (pointsByYou3 / totalPoints3) * (percentage3);

                    //category 4
                    finalGrade += (pointsByYou4/totalPoints4) * (percentage4);

                    totPercent = percentage + percentage2 + percentage3 + percentage4;
                }
                break;
            case 5:
                if(!checkVal)
                {
                    totalPoints = 0;
                    pointsByYou = 0;
                    percentage = 0;
                    totalPoints2 = 0;
                    pointsByYou2 = 0;
                    percentage2 = 0;
                    totalPoints3 = 0;
                    pointsByYou3 = 0;
                    percentage3 = 0;
                    totalPoints4 = 0;
                    pointsByYou4 = 0;
                    percentage4 = 0;
                    totalPoints5 = 0;
                    pointsByYou5 = 0;
                    percentage5 = 0;
                    finalGrade = 0;
                }
                else
                {
                    //category 1
                    finalGrade = (pointsByYou / totalPoints) * (percentage);

                    //category 2
                    finalGrade += (pointsByYou2 / totalPoints2) * (percentage2);

                    //category 3
                    finalGrade += (pointsByYou3 / totalPoints3) * (percentage3);

                    //category 4
                    finalGrade += (pointsByYou4/totalPoints4) * (percentage4);

                    //category 5
                    finalGrade += (pointsByYou5/totalPoints5) * (percentage5);

                    totPercent = percentage + percentage2 + percentage3 + percentage4 + percentage5;
                }
                break;
            case 6:
                if(!checkVal)
                {
                    totalPoints = 0;
                    pointsByYou = 0;
                    percentage = 0;
                    totalPoints2 = 0;
                    pointsByYou2 = 0;
                    percentage2 = 0;
                    totalPoints3 = 0;
                    pointsByYou3 = 0;
                    percentage3 = 0;
                    totalPoints4 = 0;
                    pointsByYou4 = 0;
                    percentage4 = 0;
                    totalPoints5 = 0;
                    pointsByYou5 = 0;
                    percentage5 = 0;
                    totalPoints6 = 0;
                    pointsByYou6 = 0;
                    percentage6 = 0;
                    finalGrade = 0;
                }
                else
                {
                    //category 1
                    finalGrade = (pointsByYou / totalPoints) * (percentage);

                    //category 2
                    finalGrade += (pointsByYou2 / totalPoints2) * (percentage2);

                    //category 3
                    finalGrade += (pointsByYou3 / totalPoints3) * (percentage3);

                    //category 4
                    finalGrade += (pointsByYou4/totalPoints4) * (percentage4);

                    //category 5
                    finalGrade += (pointsByYou5/totalPoints5) * (percentage5);

                    //category 6
                    finalGrade += (pointsByYou6/totalPoints6) * (percentage6);

                    totPercent = percentage + percentage2 + percentage3 + percentage4 + percentage5
                            + percentage6;
                }
                break;
            case 7:
                if(!checkVal)
                {
                    totalPoints = 0;
                    pointsByYou = 0;
                    percentage = 0;
                    totalPoints2 = 0;
                    pointsByYou2 = 0;
                    percentage2 = 0;
                    totalPoints3 = 0;
                    pointsByYou3 = 0;
                    percentage3 = 0;
                    totalPoints4 = 0;
                    pointsByYou4 = 0;
                    percentage4 = 0;
                    totalPoints5 = 0;
                    pointsByYou5 = 0;
                    percentage5 = 0;
                    totalPoints6 = 0;
                    pointsByYou6 = 0;
                    percentage6 = 0;
                    finalGrade = 0;
                }
                else
                {
                    //category 1
                    finalGrade = (pointsByYou / totalPoints) * (percentage);

                    //category 2
                    finalGrade += (pointsByYou2 / totalPoints2) * (percentage2);

                    //category 3
                    finalGrade += (pointsByYou3 / totalPoints3) * (percentage3);

                    //category 4
                    finalGrade += (pointsByYou4/totalPoints4) * (percentage4);

                    //category 5
                    finalGrade += (pointsByYou5/totalPoints5) * (percentage5);

                    //category 6
                    finalGrade += (pointsByYou6/totalPoints6) * (percentage6);

                    //category 7
                    finalGrade += (pointsByYou7/totalPoints7) * (percentage7);

                    totPercent = percentage + percentage2 + percentage3 + percentage4 + percentage5
                            + percentage6 + percentage7;
                }
                break;
            case 8:
                if(!checkVal)
                {
                    totalPoints = 0;
                    pointsByYou = 0;
                    percentage = 0;
                    totalPoints2 = 0;
                    pointsByYou2 = 0;
                    percentage2 = 0;
                    totalPoints3 = 0;
                    pointsByYou3 = 0;
                    percentage3 = 0;
                    totalPoints4 = 0;
                    pointsByYou4 = 0;
                    percentage4 = 0;
                    totalPoints5 = 0;
                    pointsByYou5 = 0;
                    percentage5 = 0;
                    totalPoints6 = 0;
                    pointsByYou6 = 0;
                    percentage6 = 0;
                    finalGrade = 0;
                }
                else
                {
                    //category 1
                    finalGrade = (pointsByYou / totalPoints) * (percentage);

                    //category 2
                    finalGrade += (pointsByYou2 / totalPoints2) * (percentage2);

                    //category 3
                    finalGrade += (pointsByYou3 / totalPoints3) * (percentage3);

                    //category 4
                    finalGrade += (pointsByYou4/totalPoints4) * (percentage4);

                    //category 5
                    finalGrade += (pointsByYou5/totalPoints5) * (percentage5);

                    //category 6
                    finalGrade += (pointsByYou6/totalPoints6) * (percentage6);

                    //category 7
                    finalGrade += (pointsByYou7/totalPoints7) * (percentage7);

                    //category 8
                    finalGrade += (pointsByYou8/totalPoints8) * (percentage8);

                    totPercent = percentage + percentage2 + percentage3 + percentage4 + percentage5
                            + percentage6 + percentage7 + percentage8;

                }
                break;
        }

    }

    //returns the final grade
    public double getFinalGrade()
    {
        return finalGrade;
    }

    //removes one table row at a time
    private void removeOneFullTableRow(int count)
    {
        switch(count)
        {
            case 2:
                tableRow2.setVisibility(View.GONE);
                break;
            case 3:
                tableRow3.setVisibility(View.GONE);
                break;
            case 4:
                tableRow4.setVisibility(View.GONE);
                break;
            case 5:
                tableRow5.setVisibility(View.GONE);
                break;
            case 6:
                tableRow6.setVisibility(View.GONE);
                break;
            case 7:
                tableRow7.setVisibility(View.GONE);
                break;
            case 8:
                tableRow8.setVisibility(View.GONE);
                break;
        }
    }

    //changes the table row from visible to gone
    private void clearTableRow(int count)
    {
        switch(count)
        {
            case 2:
                tableRow2.setVisibility(View.GONE);
                break;
            case 3:
                tableRow2.setVisibility(View.GONE);
                tableRow3.setVisibility(View.GONE);
                break;
            case 4:
                tableRow2.setVisibility(View.GONE);
                tableRow3.setVisibility(View.GONE);
                tableRow4.setVisibility(View.GONE);
                break;
            case 5:
                tableRow2.setVisibility(View.GONE);
                tableRow3.setVisibility(View.GONE);
                tableRow4.setVisibility(View.GONE);
                tableRow5.setVisibility(View.GONE);
                break;
            case 6:
                tableRow2.setVisibility(View.GONE);
                tableRow3.setVisibility(View.GONE);
                tableRow4.setVisibility(View.GONE);
                tableRow5.setVisibility(View.GONE);
                tableRow6.setVisibility(View.GONE);
                break;
            case 7:
                tableRow2.setVisibility(View.GONE);
                tableRow3.setVisibility(View.GONE);
                tableRow4.setVisibility(View.GONE);
                tableRow5.setVisibility(View.GONE);
                tableRow6.setVisibility(View.GONE);
                tableRow7.setVisibility(View.GONE);
                break;
            case 8:
                tableRow2.setVisibility(View.GONE);
                tableRow3.setVisibility(View.GONE);
                tableRow4.setVisibility(View.GONE);
                tableRow5.setVisibility(View.GONE);
                tableRow6.setVisibility(View.GONE);
                tableRow7.setVisibility(View.GONE);
                tableRow8.setVisibility(View.GONE);
                break;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
