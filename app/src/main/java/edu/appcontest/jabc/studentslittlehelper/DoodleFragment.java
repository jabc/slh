package edu.appcontest.jabc.studentslittlehelper;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class DoodleFragment extends Fragment {
    private DoodleView doodleView;
    //These variables are used to calculate changes in the acceleration to
    //determine whether a shake event has taken place.
    private float acceleration;
    private float currentAcceleration;
    private float lastAcceleration;
    private boolean erase = false;
    //Use this to prevent multiple dialogs on screen at once.  For example,
    //if the ChooseColor dialog is displayed and the user accidentally shakes the device,
    //the dialog for erasing the image should not be displayed.
    private boolean dialogOnScreen = false;

    //Value used to determine whether the user shook the device to erase
    private static final int ACCELERATION_THRESHOLD = 100000;


    public DoodleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doodle, container, false);

        setHasOptionsMenu(true);

        doodleView = (DoodleView)view.findViewById(R.id.doodleView);

        acceleration = 0.0f;
        currentAcceleration = SensorManager.GRAVITY_EARTH;
        lastAcceleration = SensorManager.GRAVITY_EARTH;


        return view;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        enableAccelerometerListening();
    }

    public void enableAccelerometerListening()
    {
        //get the Sensormanager
        SensorManager sensorManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);

        //register to listen for accelerometer events
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onPause()
    {
        super.onPause();
        disableAccelerometerListening();
    }

    public void disableAccelerometerListening()
    {
        // get the SensorManager
        SensorManager sensorManager =
                (SensorManager) getActivity().getSystemService(
                        Context.SENSOR_SERVICE);

        // stop listening for accelerometer events
        sensorManager.unregisterListener(sensorEventListener,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));

    }

    private SensorEventListener sensorEventListener = new SensorEventListener()
    {//use accelerometer to determine whether user shook device
        @Override
        public void onSensorChanged(SensorEvent event)
        {
            // ensure that other dialogs are not displayed
            if (!dialogOnScreen) {
                // get x, y, and z values for the SensorEvent
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];

                // save previous acceleration value
                lastAcceleration = currentAcceleration;

                // calculate the current acceleration
                currentAcceleration = x * x + y * y + z * z;

                // calculate the change in acceleration
                acceleration = currentAcceleration *
                        (currentAcceleration - lastAcceleration);

                // if the acceleration is above a certain threshold
                if (acceleration > ACCELERATION_THRESHOLD)
                    confirmErase();
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

    };

    public void setErase(boolean e){
        erase = e;
    }

    public void confirmErase()
    {
        EraseImageDialogFragment fragment = new EraseImageDialogFragment();
        fragment.show(getFragmentManager(), "erase dialog");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.doodle_fragment_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // switch based on the MenuItem id
        switch (item.getItemId())
        {
            case R.id.color:
                ColorDialogFragment colorDialog = new ColorDialogFragment();
                colorDialog.show(getFragmentManager(), "color dialog");
                return true; // consume the menu event
            case R.id.lineWidth:
                LineWidthDialogFragment widthdialog =
                        new LineWidthDialogFragment();
                widthdialog.show(getFragmentManager(), "line width dialog");
                return true; // consume the menu event
            case R.id.eraser:
                doodleView.setDrawingColor(Color.WHITE); // line color white
                return true; // consume the menu event
            case R.id.clear:
                confirmErase(); // confirm before erasing image
                return true; // consume the menu event
            case R.id.save:
                doodleView.saveImage(); // save the current image
                return true; // consume the menu event
            case R.id.save_exit:
                doodleView.saveImage();
                getFragmentManager().popBackStackImmediate();
                getActivity().finish();
            case R.id.exit_nosave:
                //confirmErase();
                //if(erase) {
                    getActivity().finish();
                //}
        } // end switch

        return super.onOptionsItemSelected(item); // call super's method

    }
    // indicates whether a dialog is displayed
    public void setDialogOnScreen(boolean visible) {
        dialogOnScreen = visible;
    }

    public DoodleView getDoodleView()
    {return doodleView;}

}
