package edu.appcontest.jabc.studentslittlehelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by jmoralesowen on 3/16/2016.
 */
public class EraseImageDialogFragment extends DialogFragment{
    public EraseImageDialogFragment() {
        // Required empty public constructor
    }

    // create an AlertDialog and return it
    @Override
    public Dialog onCreateDialog(Bundle bundle)
    {

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());

        // set the AlertDialog's message
        builder.setMessage(R.string.message_erase);
        builder.setCancelable(false);

        // add Erase Button
        builder.setPositiveButton(R.string.button_erase,
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                        //getDoodleFragment().setErase(true);
                        getDoodleFragment().getDoodleView().clear(); // clear image
                        //dismiss();
                    }
                }
        ); // end call to setPositiveButton

        // add Cancel Button
        builder.setNegativeButton(R.string.button_cancel, null);

        return builder.create(); // return dialog
    } // end method onCreateDialog


    // tell DoodleFragment that dialog is now displayed
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        DoodleFragment fragment = getDoodleFragment();

        if (fragment != null)
            fragment.setDialogOnScreen(true);
    }

    // tell DoodleFragment that dialog is no longer displayed
    @Override
    public void onDetach()
    {
        super.onDetach();
        DoodleFragment fragment = getDoodleFragment();

        if (fragment != null)
            fragment.setDialogOnScreen(false);
    }

    // gets a reference to the DoodleFragment
    private DoodleFragment getDoodleFragment()
    {
        return (DoodleFragment)getFragmentManager().findFragmentById(R.id.doodleFragment);
    }

}
